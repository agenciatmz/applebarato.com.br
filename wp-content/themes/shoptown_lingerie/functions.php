<?php
/**
 * TemplateMela
 * @copyright  Copyright (c) TemplateMela. (http://www.templatemela.com)
 * @license    http://www.templatemela.com/license/
 * @author         TemplateMela
 * @version        Release: 1.0
 */
/**  Set Default options : Theme Settings  */

function shoptown_set_default_options_child()
{ 
	/*  General Settings  */
		
	add_option("tm_button_hover_color","ff138c"); // button hover color
	add_option("tm_revslider_alias",'tm_homeslider_lingerie');
	
	
	/*  Top Bar Settings  */	
	add_option("tm_topbar_bkg_color","FFFFFF"); // topbar_bkg_color
	add_option("tm_topbar_link_hover_color","ff138c"); // topbar_link_hover_color	
	
	/*  Navbar Setting  */
	add_option("tm_top_nav_bg_color","ffffff"); // Top menu background color
	/*  Navigation Menu Setting  */

	add_option("tm_top_menu_text_color","000000"); // Top menu text color
	add_option("tm_top_menu_texthover_color","ff138c"); // Top menu text hover color
	
	/*  Content Settings  */
	add_option("tm_hoverlink_color","ff138c"); // link hover color
	
	/*  Footer Settings  */	
	add_option("tm_footerhoverlink_color","ff138c"); // footer link hover text color
}	
add_action('init', 'shoptown_set_default_options_child');
function shoptown_child_scripts() {
    wp_enqueue_style( 'shoptown-child-style', get_template_directory_uri(). '/style.css' );	
}
add_action( 'wp_enqueue_scripts', 'shoptown_child_scripts' );
function shoptown_load_scripts_child() {	
wp_enqueue_script( 'shoptown_custom', get_stylesheet_directory_uri() . '/js/megnor/custom.js', array(), '', true);	  	
 }
add_action( 'wp_enqueue_scripts', 'shoptown_load_scripts_child' );
function shoptown_remove_child_widgets(){
    unregister_sidebar( 'home-sidebar' );
	unregister_sidebar( 'home-banners' );
}
add_action( 'widgets_init', 'shoptown_remove_child_widgets',15 );


?>

